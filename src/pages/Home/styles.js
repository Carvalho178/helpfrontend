import styled, { keyframes } from 'styled-components';
import { Link } from 'react-router-dom';

import construction from '../../assets/images/banner/construction.jpg';
import mechanic from '../../assets/images/banner/mechanic.jpeg';
import teacher from '../../assets/images/banner/teacher.jpeg';

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
`;

const banners = keyframes`
  0%, 40% {
    background: url(${mechanic}) no-repeat center center
    background-size: cover;
  }
  40%, 70% {
    background: url(${teacher}) no-repeat center center
    background-size: cover;
  }

  70%, 100% {
    background: url(${construction}) no-repeat center center
    background-size: cover;
  }
`;

export const Banner = styled.div`
  height: 350px;
  animation: ${banners} 15s infinite;
  animation-direction: alternate;
  display: flex;
  flex-direction: column;
  align-items: center;

  div {
    margin-top: 200px;
    display: flex;
    flex-direction: column;
    align-items: center;
    position: absolute;

    strong {
      color: #fff;
      font-size: 30px;
      -webkit-text-stroke: 1px #333;
    }
    small {
      -webkit-text-stroke: 1px #333;
      margin-top: 10px;
      color: #fff;
      font-size: 28px;
      font-weight: bold;
    }
  }
`;

export const Main = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  margin-top: 30px;
`;

export const SearchPro = styled(Link)`
  text-decoration: none;
  border: 1px solid #0670fa;
  background: #0670fa;
  border-radius: 30px;
  padding: 10px;
  font-weight: bold;
  color: #fff;
`;

export const Professional = styled(Link)`
  text-decoration: none;
  border: 2px solid #ff0000;
  background: #ebebeb;
  border-radius: 30px;
  padding: 10px;
  font-weight: bold;
  color: #ff0000;
`;

export const Schedule = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  height: 100%;
  align-items: center;
  justify-content: space-around;

  h1 {
    padding: 10px;
    font-size: 20px;
    color: #999;
  }

  strong {
    padding: 10px;
  }

  button {
    border: none;
    background: none;
    font-size: 20px;
    color: #0670fa;
  }
`;

export const Appointment = styled.div`
  display: flex;
  width: 100vw;
  height: 100%;
  overflow: auto;
  padding: 30px;
  margin: auto;

  div {
    width: 100%;

    display: flex;
    align-items: center;
    justify-content: space-around;
    border: 1px solid #999;
    border-radius: 6px;
    margin: 0 5px;

    img {
      height: 180px;
    }
  }
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  height: 180px;

  stron {
    width: 100%;
    font-size: 14px;
  }

  small {
    width: 100%;
    font-size: 12px;
  }
`;
