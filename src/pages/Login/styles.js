import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  flex: 1;
  height: 100vh;
  img {
    height: 120px;
  }
`;

export const Form = styled.form`
  display: flex;
  width: 100%;
  max-width: 500px;
  flex-direction: column;
  background: #fff;
  padding: 15px;
  margin-top: 15px;
  border-radius: 6px;

  strong {
    font-size: 18px;
    font-weight: normal;
    margin: 8px 0;
    color: #333;
  }

  input {
    font-size: 16px;
    padding: 8px;
    border: 1px solid #aaa;
    border-radius: 6px;

    &:focus {
      border: 2px solid #0670fa;
    }
  }
`;

export const Password = styled.input`
  font-size: 16px;
  padding: 8px;
  border: 1px solid #aaa;
  border-radius: 6px;
  -webkit-text-security: disc;

  &:focus {
    border: 2px solid #0670fa;
  }
`;

export const Buttons = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 10px 0;
`;
export const SubmitButton = styled.button`
  background: #0670fa;
  border: 0;
  padding: 8px;
  border-radius: 6px;
  color: #fff;
  font-size: 16px;
`;

export const Register = styled(Link)`
  text-decoration: none;
  color: #0670fa;
`;
