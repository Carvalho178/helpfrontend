import styled from 'styled-components';

export const Container = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 100%;
  padding: 0 30px;
  margin: auto;

  img {
    margin: 7px 0;
    height: 34px;
  }
`;
