/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import api from '../../services/api';
import Login from '../Login';

import {
  Container,
  Welcome,
  Form,
  Service,
  SubmitButton,
  Sucess,
  Background,
} from './styles';

class RegisterPro extends Component {
  state = {
    categories: [],
    description: '',
    occupation: '',
    price_hour: '',
    category_id: 0,
    initial_hour: '',
    final_hour: '',
    sucess: false,
    request_error: false,
    isProvider: false,
  };

  async componentDidMount() {
    const { user } = this.props;
    if (user) {
      const provider = await api.get(`providers/user/${user.id}`);
      if (provider.data) {
        this.setState({ isProvider: true });
      }
    }

    const response = await api.get('categories');

    this.setState({
      categories: response.data,
    });
  }

  handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = async e => {
    const {
      description,
      occupation,
      price_hour,
      category_id,
      initial_hour,
      final_hour,
    } = this.state;
    const { user } = this.props;

    let sucess = false;
    let request_error = false;

    e.preventDefault();

    await api
      .post('/providers', {
        description,
        occupation,
        price_hour,
        category_id,
        initial_hour,
        final_hour,
        user_id: user.id,
      })
      .then(() => {
        sucess = true;
      })
      .catch(() => {
        request_error = true;
      });

    this.setState({
      sucess,
      request_error,
    });
  };

  render() {
    const { user } = this.props;
    const {
      categories,
      description,
      occupation,
      price_hour,
      sucess,
      request_error,
      initial_hour,
      final_hour,
      isProvider,
    } = this.state;

    if (!user) {
      return <Login />;
    }

    if (sucess) {
      return (
        <Sucess>
          <strong>Atualizacao realizada com sucesso!</strong>
          <small>Aproveite nossa plataforma!</small>
        </Sucess>
      );
    }

    if (isProvider) {
      return (
        <Sucess>
          <strong>Você já faz parte do nosso time!</strong>
          <small>Espere notificações dos seus agendamentos!</small>
        </Sucess>
      );
    }

    return (
      <Background>
        <Container requestError={request_error}>
          <Welcome>
            <strong>Deseja se prestar serviços?</strong>
            <small>Insira seus dados e continue nessa jornada :)</small>
          </Welcome>
          <Form onSubmit={this.handleSubmit}>
            <strong>Descrição</strong>
            <textarea
              type="text"
              size="38"
              placeholder="Descreva suas competencias"
              name="description"
              value={description}
              onChange={this.handleInputChange}
            />
            <Service>
              <div>
                <strong>Profissao</strong>
                <input
                  type="text"
                  placeholder="Digite sua cidade"
                  name="occupation"
                  value={occupation}
                  onChange={this.handleInputChange}
                />
              </div>
              <div>
                <strong>Categoria</strong>
                <select
                  name="category_id"
                  id="category"
                  onChange={this.handleInputChange}
                >
                  {categories.map(category => (
                    <option key={String(category.id)} value={category.id}>
                      {category.name}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <strong>Preço/hora</strong>
                <input
                  type="text"
                  placeholder="Digite o valor por hora"
                  name="price_hour"
                  value={price_hour}
                  onChange={this.handleInputChange}
                />
              </div>
              <div>
                <strong>Hora Inicial</strong>
                <input
                  type="time"
                  name="initial_hour"
                  value={initial_hour}
                  onChange={this.handleInputChange}
                />
              </div>
              <div>
                <strong>Hora Final</strong>
                <input
                  type="time"
                  name="final_hour"
                  value={final_hour}
                  onChange={this.handleInputChange}
                />
              </div>
            </Service>
            <SubmitButton>Enviar</SubmitButton>
          </Form>
        </Container>
      </Background>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(RegisterPro);
