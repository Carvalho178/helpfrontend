export function loginUser(user) {
  return {
    type: '@user/LOGIN',
    user,
  };
}
