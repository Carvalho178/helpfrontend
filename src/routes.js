import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import Register from './pages/Register';
import SearchPro from './pages/SearchPro';
import Provider from './pages/Provider';
import RegisterPro from './pages/RegisterPro';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/register" exact component={Register} />
      <Route path="/register/provider" exact component={RegisterPro} />
      <Route path="/provider" exact component={SearchPro} />
      <Route path="/provider/:provider_id" component={Provider} />
    </Switch>
  );
}
