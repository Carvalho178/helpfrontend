/* eslint-disable react/prop-types */
/* eslint-disable-next-line */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import api from '../../services/api';
import nophoto from '../../assets/images/nophoto.jpg';

import Login from '../Login';

import {
  Container,
  Banner,
  Main,
  SearchPro,
  Professional,
  Schedule,
  Appointment,
  Info,
} from './styles';

class Home extends Component {
  state = {
    appointments: [],
    schedules: [],
    isProvider: false,
    loadAppointments: false,
  };

  handleLoadAppointments = async () => {
    const { user } = this.props;

    this.setState({
      loadAppointments: true,
    });

    const provider = await api.get(`/providers/user/${user.id}`);

    if (provider.data) {
      this.setState({
        isProvider: true,
      });

      const schedules = await api.get(`/schedule/${provider.data.id}`);

      if (schedules.data) {
        this.setState({
          schedules: schedules.data,
        });
      }
    }

    const appointments = await api.get(`appointments/${user.id}`);

    if (appointments.data) {
      this.setState({
        appointments: appointments.data,
      });
    }
  };

  render() {
    const { user = {} } = this.props;

    const {
      isProvider,
      appointments,
      schedules,
      loadAppointments,
    } = this.state;

    return user.id ? (
      <Container>
        <Banner>
          <div>
            <strong>Precisa de alguma ajuda?</strong>
            <small>Pede um Help</small>
          </div>
        </Banner>
        <Main>
          <SearchPro to="/provider">Buscar Profissionasis</SearchPro>
          <Professional to="/register/provider">Sou Profissional</Professional>
        </Main>
        <Schedule>
          <div>
            <button type="button" onClick={() => this.handleLoadAppointments()}>
              Carregar Compromissos
            </button>
          </div>
          {appointments.length > 0 || schedules.length > 0 ? (
            <>
              <strong>Compromissos com Profissionais</strong>
              {appointments.length > 0 ? (
                <Appointment>
                  {appointments.map(appointment => (
                    <div key={String(appointment.id)}>
                      <>
                        <img
                          src={
                            appointment.Provider.User.File
                              ? appointment.Provider.User.File.url
                              : nophoto
                          }
                          alt=""
                        />
                      </>
                      <Info>
                        <>
                          <strong>{appointment.Provider.User.name}</strong>
                          <small>{appointment.city}</small>
                          <small>{appointment.address}</small>
                          <small>{appointment.complement}</small>
                          <small>{appointment.date}</small>
                        </>
                      </Info>
                    </div>
                  ))}
                </Appointment>
              ) : (
                <h1>Você não possui compromissos com profissonais</h1>
              )}
              {isProvider &&
                (schedules.length > 0 ? (
                  <>
                    <strong>Compromissos com Clientes</strong>
                    <Appointment>
                      {schedules.map(schedule => (
                        <div key={String(schedule.id)}>
                          <>
                            <img
                              src={
                                schedule.User.File
                                  ? schedule.User.File.url
                                  : nophoto
                              }
                              alt=""
                            />
                          </>
                          <Info>
                            <>
                              <strong>{schedule.User.name}</strong>
                              <small>{schedule.city}</small>
                              <small>{schedule.address}</small>
                              <small>{schedule.complement}</small>
                              <small>{schedule.date}</small>
                            </>
                          </Info>
                        </div>
                      ))}
                    </Appointment>
                  </>
                ) : (
                  <h1>Você não possui compromissos com clientes</h1>
                ))}
            </>
          ) : (
            loadAppointments && (
              <>
                <h1>Você não possui compromissos</h1>
              </>
            )
          )}
        </Schedule>
      </Container>
    ) : (
      <Login />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(Home);
