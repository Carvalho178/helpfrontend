/* eslint-disable react/prop-types */
import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container } from './styles';
import logoblue from '../../assets/images/logoblue.svg';

function Header({ user = {} }) {
  return user.id ? (
    <Container>
      <Link to="/">
        <img src={logoblue} alt="logo" />
      </Link>
      <div>{user.name}</div>
    </Container>
  ) : (
    <Container />
  );
}

const mapStateToProps = state => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(Header);
