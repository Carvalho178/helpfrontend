/* eslint-disable no-unused-expressions */
export default function user(state = {}, action) {
  const actions = {
    '@user/LOGIN': action.user,
  };

  return actions[action.type] ? actions[action.type] : state;
}
