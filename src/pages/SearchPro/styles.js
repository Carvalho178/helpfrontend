import styled from 'styled-components';

export const Container = styled.div`
  min-height: calc(100vh - 48px);
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 30px;
`;

export const Search = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100ch;
  width: 100%;
  max-width: 800px;

  form {
    width: 100%;
    display: flex;

    input {
      width: 100%;
      font-size: 16px;
      padding: 8px;
      border: 1px solid #aaa;
      border-radius: 6px;

      &:focus {
        border: 2px solid #0670fa;
      }
    }

    button {
      margin-left: 7px;
      background: #0670fa;
      padding: 8px;
      border-radius: 6px;
      color: #fff;
      font-size: 16px;
    }
  }
`;

export const Filters = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-evenly;
  margin: 10px 0;
  padding: 30px;
  border-bottom: 1px solid #333;
`;

export const Result = styled.div`
  overflow: auto;
  width: 100%;
  padding: 10px 0;
`;

export const Provider = styled.div`
  text-decoration: none;
  color: #444;
  display: flex;
  flex-direction: row;
  width: 100%;
  padding: 20px 0;
  border-bottom: 1px solid #333;
  margin: 10px 9;
  div {
    display: flex;
    width: 230px;
    img {
      background: #aaa;
      height: 96px;
      width: 96px;
      border-radius: 6px;
    }
  }
  p {
    font-size: 16px;
    margin-left: 5px;
    height: 96px;
    width: 100%;
    overflow: auto;
  }
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  overflow: hidden;
  margin-left: 5px;

  width: 100%;
  height: 96px;
  h1 {
    font-size: 20px;
    text-overflow: ellipsis;
  }

  small {
    font-size: 14px;
  }
  strong {
    font-size: 14px;
  }
`;
