/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Login from '../Login';
import api from '../../services/api';
import nophoto from '../../assets/images/nophoto.jpg';

import { Container, Page, Info, Options, Form, Place, Sucess } from './styles';

class Provider extends Component {
  state = {
    provider: {},
    states: [],
    address: '',
    city: '',
    complement: '',
    cep: '',
    date: '',
    state_id: 0,
    sucess: false,
    request_error: false,
  };

  async componentDidMount() {
    const { match } = this.props;
    const { provider_id } = match.params;

    const response = await api.get(`/providers/${provider_id}`);
    const estados = await api.get('states');

    this.setState({ provider: response.data, states: estados.data });
  }

  handleDataUser = async e => {
    e.preventDefault();

    const { user } = this.props;
    if (user) {
      this.setState({
        address: user.address,
        city: user.city,
        complement: user.complement,
        cep: user.cep,
        state_id: user.state_id,
      });
    }
  };

  handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = async e => {
    const { user } = this.props;

    const {
      provider,
      address,
      city,
      complement,
      cep,
      state_id,
      date,
    } = this.state;

    let sucess = false;
    let request_error = false;

    e.preventDefault();

    await api
      .post('/appointments', {
        provider_id: provider.id,
        user_id: user.id,
        address,
        city,
        complement,
        cep,
        state_id,
        date,
      })
      .then(() => {
        sucess = true;
      })
      .catch(() => {
        request_error = true;
      });

    this.setState({
      sucess,
      request_error,
    });
  };

  render() {
    const { user } = this.props;

    const {
      provider,
      address,
      complement,
      city,
      states,
      cep,
      date,
      request_error,
      sucess,
    } = this.state;

    if (!user) {
      return <Login />;
    }

    return provider.User ? (
      <Container>
        <Page>
          <div>
            <img
              src={provider.User.File ? provider.User.File.url : nophoto}
              alt={provider.User.name}
            />
            <Info>
              <h1>{provider.occupation}</h1>
              <h1>{provider.User.name}</h1>
              <>
                <small>{provider.User.city}</small>
                <small>{provider.User.address}</small>
              </>
              <small>Cel: {provider.User.cell_phone}</small>
              <strong>R$ {provider.price_hour}/hora</strong>
            </Info>
          </div>
          <p>{provider.description}</p>
          <Form onSubmit={this.handleSubmit} requestError={request_error}>
            <strong>Endereço</strong>
            <input
              type="text"
              placeholder="Digite seu endereço"
              name="address"
              value={address}
              onChange={this.handleInputChange}
            />
            <strong>Complemento</strong>
            <input
              type="text"
              placeholder="Digite seu complemento"
              name="complement"
              value={complement}
              onChange={this.handleInputChange}
            />
            <Place>
              <div>
                <strong>Cidade</strong>
                <input
                  type="text"
                  placeholder="Digite sua cidade"
                  name="city"
                  value={city}
                  onChange={this.handleInputChange}
                />
              </div>
              <div>
                <strong>Estado</strong>
                <select
                  name="state_id"
                  id="state"
                  onChange={this.handleInputChange}
                >
                  {states.map(state => (
                    <option key={String(state.id)} value={state.id}>
                      {state.name} - {state.uf}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <strong>CEP</strong>
                <input
                  type="text"
                  placeholder="Digite seu cep"
                  name="cep"
                  value={cep}
                  onChange={this.handleInputChange}
                />
              </div>
            </Place>
            <strong>Data/Hora</strong>
            <input
              type="datetime-local"
              name="date"
              value={date}
              onChange={this.handleInputChange}
            />
            <Options>
              <button type="submit">Chamar</button>
              <button type="button" onClick={this.handleDataUser}>
                Meu Endereço
              </button>
            </Options>
          </Form>
        </Page>
        <Sucess sucess={sucess}>
          <strong>Sua chamada foi efetuada!</strong>
          <small>O {provider.User.name} será chamado!!</small>
          <button
            type="button"
            onClick={() => {
              this.setState({ sucess: false });
            }}
          >
            Ok
          </button>
        </Sucess>
      </Container>
    ) : (
      <Container />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(Provider);
