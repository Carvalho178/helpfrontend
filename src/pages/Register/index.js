/* eslint-disable camelcase */
import React, { Component } from 'react';
import api from '../../services/api';
import logoblue from '../../assets/images/logoblue.svg';

import {
  Container,
  Welcome,
  Form,
  Password,
  Place,
  SubmitButton,
  Phone,
  InputPhoto,
  Contact,
  Sucess,
  Login,
  Logo,
  Background,
} from './styles';

export default class Register extends Component {
  state = {
    states: [],
    name: '',
    email: '',
    cpfcnpj: '',
    birth_date: '',
    password: '',
    telephone: '',
    cell_phone: '',
    address: '',
    city: '',
    complement: '',
    cep: '',
    state_id: 0,
    sucess: false,
    request_error: false,
  };

  async componentDidMount() {
    const response = await api.get('states');

    this.setState({
      states: response.data,
    });
  }

  handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = async e => {
    const {
      name,
      email,
      cpfcnpj,
      birth_date,
      password,
      telephone,
      cell_phone,
      address,
      city,
      complement,
      cep,
      state_id,
    } = this.state;

    let sucess = false;
    let request_error = false;

    e.preventDefault();

    await api
      .post('/users', {
        name,
        email,
        cpfcnpj,
        birth_date,
        password,
        telephone,
        cell_phone,
        address,
        city,
        complement,
        cep,
        state_id,
      })
      .then(() => {
        sucess = true;
      })
      .catch(() => {
        request_error = true;
      });

    this.setState({
      sucess,
      request_error,
    });
  };

  render() {
    const {
      states,
      name,
      email,
      cpfcnpj,
      birth_date,
      password,
      telephone,
      cell_phone,
      address,
      city,
      complement,
      cep,
      sucess,
      request_error,
    } = this.state;

    if (sucess) {
      return (
        <Sucess>
          <strong>Conta criada com sucesso!</strong>
          <small>Aproveite nossa plataforma!</small>
          <Login to="/">Efetuar login</Login>
        </Sucess>
      );
    }

    return (
      <Background>
        <Logo to="/">
          <img src={logoblue} alt="logo" />
        </Logo>
        <Container requestError={request_error}>
          <Welcome>
            <strong>Seja bem vindo</strong>
            <small>Insira seus dados e aproveite nossa plataforma :)</small>
          </Welcome>
          <Form onSubmit={this.handleSubmit}>
            <strong>Nome Completo</strong>
            <input
              type="text"
              placeholder="Seu nome completo"
              name="name"
              value={name}
              onChange={this.handleInputChange}
            />
            <strong>CPF ou CNPJ</strong>
            <input
              type="text"
              placeholder="Digite seu cpf ou cnpj"
              name="cpfcnpj"
              value={cpfcnpj}
              onChange={this.handleInputChange}
            />
            <strong>Data de nascimento</strong>
            <input
              type="date"
              name="birth_date"
              value={birth_date}
              onChange={this.handleInputChange}
            />
            <Contact>
              <div>
                <strong>Login</strong>
                <input
                  type="text"
                  placeholder="Digite seu email"
                  name="email"
                  value={email}
                  onChange={this.handleInputChange}
                />
              </div>
              <div>
                <strong>Senha</strong>
                <Password
                  type="text"
                  placeholder="Digite sua senha"
                  name="password"
                  value={password}
                  onChange={this.handleInputChange}
                />
              </div>
            </Contact>
            <Phone>
              <div>
                <strong>Telefone</strong>
                <input
                  type="text"
                  placeholder="Digite seu telefone"
                  name="telephone"
                  value={telephone}
                  onChange={this.handleInputChange}
                />
              </div>
              <div>
                <strong>Celular</strong>
                <input
                  type="text"
                  placeholder="Digite seu celular"
                  name="cell_phone"
                  value={cell_phone}
                  onChange={this.handleInputChange}
                />
              </div>
            </Phone>
            <strong>Endereço</strong>
            <input
              type="text"
              placeholder="Digite seu endereço"
              name="address"
              value={address}
              onChange={this.handleInputChange}
            />
            <strong>Complemento</strong>
            <input
              type="text"
              placeholder="Digite seu complemento"
              name="complement"
              value={complement}
              onChange={this.handleInputChange}
            />
            <Place>
              <div>
                <strong>Cidade</strong>
                <input
                  type="text"
                  placeholder="Digite sua cidade"
                  name="city"
                  value={city}
                  onChange={this.handleInputChange}
                />
              </div>
              <div>
                <strong>Estado</strong>
                <select
                  name="state_id"
                  id="state"
                  onChange={this.handleInputChange}
                >
                  {states.map(state => (
                    <option key={String(state.id)} value={state.id}>
                      {state.name} - {state.uf}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <strong>CEP</strong>
                <input
                  type="text"
                  placeholder="Digite seu cep"
                  name="cep"
                  value={cep}
                  onChange={this.handleInputChange}
                />
              </div>
            </Place>
            <InputPhoto>
              <strong>Foto</strong>
              <input type="file" />
            </InputPhoto>
            <SubmitButton>Enviar</SubmitButton>
          </Form>
        </Container>
      </Background>
    );
  }
}
