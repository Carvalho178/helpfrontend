import styled, { css } from 'styled-components';

export const Container = styled.div`
  min-height: calc(100vh - 48px);
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
  flex-direction: column;
  overflow: auto;
  padding: 30px;
`;

export const Page = styled.div`
  display: flex;
  flex-direction: column;
  padding: 30px;
  background: #eee;
  border: 1px solid #333;
  border-radius: 5px;
  box-shadow: 0 4px 4px 4px #bbb;

  div {
    display: flex;
    img {
      border-radius: 6px;
      height: 200px;
    }
  }

  p {
    border-top: 1px solid #aaa;
    border-bottom: 1px solid #aaa;
    padding: 10px 5px;
    margin: 10px 0;
    font-size: 16px;
  }
`;

export const Info = styled.div`
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  overflow: auto;
  margin-left: 5px;
  width: 100%;
  height: 200px;

  h1 {
    font-size: 28px;
    color: #333;
  }
  small {
    font-size: 18px;
    color: #555;
  }
  strong {
    font-size: 20px;
  }
`;

export const Options = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  margin-top: 30px;
  width: 100%;

  button {
    margin-left: 7px;
    background: #0670fa;
    padding: 8px;
    border-radius: 12px;
    color: #fff;
    font-size: 16px;
  }
`;

export const Form = styled.form`
  display: flex;
  width: 100%;
  flex-direction: column;
  background: #efefef;
  padding: 15px;
  border-radius: 6px;
  border: ${props =>
    props.requestError
      ? css`
     3px solid #ff0000
    `
      : css`1px solid #aaa`};

  strong {
    font-size: 18px;
    font-weight: normal;
    margin: 8px 0;
    color: #333;
  }

  input {
    font-size: 16px;
    padding: 8px;
    border: 1px solid #aaa;
    border-radius: 6px;
    flex: 1;

    &:focus {
      border: 2px solid #0670fa;
    }
  }
`;
export const Place = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  align-content: flex-start;

  div {
    display: flex;
    flex-direction: column;
    flex: 1;

    & + div {
      margin-left: 5px;
    }
  }

  select {
    font-size: 16px;
    border: 1px solid #aaa;
    border-radius: 6px;
    flex: 1;

    &:focus {
      border: 2px solid #0670fa;
    }
  }
`;

export const Sucess = styled.div`
  position: absolute;
  display: ${props => (props.sucess ? css`flex` : css`none`)};
  flex-direction: column;
  align-items: center;

  background: rgba(100, 100, 100, 0.5);
  padding: 20px 50px;
  border-radius: 6px;
  box-shadow: 0 5px 5px 5px #999;

  strong {
    font-size: 24px;
    color: #0670fa;
  }

  small {
    font-size: 18px;
    color: #0670fa;
  }

  button {
    margin-top: 7px;
    background: #0670fa;
    padding: 10px;
    border-radius: 50%;
    color: #fff;
    font-size: 16px;
  }
`;
