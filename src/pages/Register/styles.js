import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

export const Background = styled.div`
  margin-top: 7px;
  padding: 0 30px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Container = styled.div`
  margin: 10px 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: #eee;
  box-shadow: 0 4px 4px 4px #bbb;
  border: ${props =>
    props.requestError
      ? css`
     1px solid #ff0000;
    `
      : css`1px solid #aaa`};
`;
export const Sucess = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  align-content: center;
  flex-direction: column;
  height: calc(100vh - 96px);

  strong {
    font-size: 24px;
  }
  small {
    font-size: 20px;
  }
`;

export const Login = styled(Link)`
  text-decoration: none;
  font-size: 20px;
  font-weight: bold;
  margin-top: 5px;
  color: #0670fa;
`;

export const Welcome = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  padding: 15px;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: #ededed;
  border-bottom: 1px solid #999;

  strong {
    font-size: 24px;
    margin-bottom: 5px;
    color: #333;
  }

  small {
    font-size: 17px;
    color: #666;
  }
`;

export const Form = styled.form`
  display: flex;
  width: 100%;
  flex-direction: column;
  background: #efefef;
  padding: 15px;
  border-radius: 6px;

  strong {
    font-size: 18px;
    font-weight: normal;
    margin: 8px 0;
    color: #333;
  }

  input {
    font-size: 16px;
    padding: 8px;
    border: 1px solid #aaa;
    border-radius: 6px;
    flex: 1;

    &:focus {
      border: 2px solid #0670fa;
    }
  }
`;

export const InputPhoto = styled.div`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  width: auto;

  input {
    padding: 0;
    margin: 0;
    width: 50%;
  }
`;

export const Contact = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  align-content: flex-start;

  div {
    display: flex;
    flex-direction: column;
    flex: 1;

    & + div {
      margin-left: 5px;
    }
  }
`;

export const Password = styled.input`
  padding: 8px;
  border: 1px solid #aaa;
  border-radius: 6px;
  -webkit-text-security: disc;

  &:focus {
    border: 2px solid #0670fa;
  }
`;

export const Phone = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  align-content: flex-start;

  div {
    display: flex;
    flex-direction: column;
    flex: 1;

    & + div {
      margin-left: 5px;
    }
  }
`;

export const Place = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  align-content: flex-start;

  div {
    display: flex;
    flex-direction: column;
    flex: 1;

    & + div {
      margin-left: 5px;
    }
  }

  select {
    font-size: 16px;
    border: 1px solid #aaa;
    border-radius: 6px;
    flex: 1;

    &:focus {
      border: 2px solid #0670fa;
    }
  }
`;

export const SubmitButton = styled.button`
  background: #0670fa;
  border: 0;
  padding: 8px;
  border-radius: 6px;
  color: #fff;
  font-size: 16px;
  margin-top: 10px;
  width: 60px;
`;

export const Logo = styled(Link)`
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    height: 48px;
  }
`;
