/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as UserActions from '../../store/modules/user/actions';
import {
  Container,
  Form,
  SubmitButton,
  Buttons,
  Register,
  Password,
} from './styles';
import logoblue from '../../assets/images/logoblue.svg';
import api from '../../services/api';

class Login extends Component {
  state = {
    email: '',
    password: '',
  };

  handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = async e => {
    const { email, password } = this.state;
    const { loginUser } = this.props;
    e.preventDefault();

    await api.post('/sessions', { email, password }).then(res => {
      loginUser(res.data);
    });
  };

  render() {
    const { email, password } = this.state;
    return (
      <Container>
        <img src={logoblue} alt="logo" />
        <Form onSubmit={this.handleSubmit}>
          <strong>Login</strong>
          <input
            type="text"
            placeholder="Digite seu email"
            name="email"
            value={email}
            onChange={this.handleInputChange}
          />
          <strong>Senha</strong>
          <Password
            type="text"
            placeholder="Digite sua senha"
            name="password"
            value={password}
            onChange={this.handleInputChange}
          />
          <Buttons>
            <Register to="/register">Cadatre-se</Register>
            <SubmitButton>Entrar</SubmitButton>
          </Buttons>
        </Form>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserActions, dispatch);

export default connect(null, mapDispatchToProps)(Login);
