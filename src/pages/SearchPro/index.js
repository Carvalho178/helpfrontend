/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Login from '../Login';
import api from '../../services/api';

import nophoto from '../../assets/images/nophoto.jpg';

import { Container, Search, Filters, Result, Provider, Info } from './styles';

class SearchPro extends Component {
  state = {
    providers: [],
  };

  async componentDidMount() {
    const response = await api.get('/providers');
    this.setState({ providers: response.data });
  }

  render() {
    const { user = {} } = this.props;
    const { providers = [] } = this.state;
    return user.id ? (
      <Container>
        <Search>
          <form
            onSubmit={e => {
              e.preventDefault();
            }}
          >
            <input type="text" />
            <button type="submit">Pesquisar</button>
          </form>
          <Filters>
            <strong>Cidade</strong>
            <strong>Relevancia</strong>
          </Filters>
          <Result>
            {providers.map(provider => (
              <Provider key={String(provider.id)}>
                <div>
                  <Link to={`/provider/${provider.id}`}>
                    <img
                      src={
                        provider.User.File ? provider.User.File.url : nophoto
                      }
                      alt={provider.User.name}
                    />
                  </Link>
                  <Info>
                    <h1>{provider.occupation}</h1>
                    <small>{provider.User.name}</small>
                    <small>{provider.User.city}</small>
                    <strong>R$ {provider.price_hour}/hora</strong>
                  </Info>
                </div>
                <p>{provider.description}</p>
              </Provider>
            ))}
          </Result>
        </Search>
      </Container>
    ) : (
      <Login />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(SearchPro);
